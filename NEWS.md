# hydroTools 0.1.5

- add `cal_Ts` and `cal_Tw`

# hydroTools 0.1.3

- ET0 models work now

# hydroTools 0.1.0

- Added a `NEWS.md` file to track changes to the package.
- fix `INFILT` data range, `Kstat` unit error reported by He YaNan
- `VIC_vegParam` works
